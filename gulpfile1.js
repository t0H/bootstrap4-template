var gulp           = require('gulp'),

    compass        = require('gulp-compass'),
    minifycss      = require('gulp-minify-css'),
    rename         = require('gulp-rename'),

    cssconcat      = require('gulp-concat-css'),
    concat         = require('gulp-concat'), // Подключаем gulp-concat (для конкатенации файлов)
    uglify         = require('gulp-uglifyjs'), // Подключаем gulp-uglifyjs (для сжатия JS)

    spritesmith    = require('gulp.spritesmith'),
    imagemin       = require('gulp-imagemin'), // Подключаем библиотеку для работы с изображениями
    pngquant       = require('imagemin-pngquant'), // Подключаем библиотеку для работы с png

    filter         = require('filter'),
    mainBowerFiles = require('main-bower-files'),
    autoprefixer   = require('gulp-autoprefixer');// Подключаем библиотеку для автоматического добавления префиксов

var livereload = require('gulp-livereload'),
    connect    = require('gulp-connect');

var plumber = require('gulp-plumber'),
    notify  = require('gulp-notify');

var del  = require('del'),
    path = require('path');

var ftp = require( 'vinyl-ftp' );
// ----------------------------------------

var onError = function (err) {
    console.log(err);
};

onError = {
    title  : 'Gulp',
    message: "Error: <%= error.message %>"
};

var files = {
    css          : 'docs.css',
    minCss       : 'docs.min.css',
    libsJS       : 'libs.js',
    js           : "script.js",
    minJs        : "script.min.js",
    compassConfig: "./config.rb"
};

var paths = {
    assets: "./assets",
    dev   : './source',
    founds: {}
};

paths.css   = path.join(paths.assets, "css");
paths.img   = path.join(paths.assets, "img");
paths.js    = path.join(paths.assets, "js");
paths.fonts = path.join(paths.assets, "fonts");

paths.devImg = path.join(paths.dev, "img");
paths.scss   = path.join(paths.dev, 'sass');
paths.tmp    = path.join(paths.dev, '.tmp');
paths.devJs  = path.join(paths.dev, 'js');

paths.founds.scss = path.join(paths.scss, '**/*.scss');
paths.founds.sass = path.join(paths.scss, '**/*.sass');
paths.founds.js   = path.join(paths.devJs, '**/*.js');
paths.founds.html = './*.html';


var compasParam = {
    //config_file: files.compassConfig,
    sass       : paths.scss,
    css        : paths.css,
    style      : 'nested', // expanded or nested or compact or compressed
    project    : './',
    image      : paths.img,
    javascripts: paths.js,
    comments   : false,
    sourcemap  : false,
    time       : true,
    logging    : false,
    debug      : false
};
// ----------------------------------------

gulp.task('connect', function () {
    return connect.server({
        root      : './',
        livereload: true
    });
});

gulp.task('html', function () {
    return gulp.src([paths.founds.html]).pipe(livereload());
});

// ----------------------------------------

gulp.task('compass', function () {//['del:css'],
    return gulp.src([paths.founds.sass])
        .pipe(plumber({errorHandler: onError}))
        .pipe(compass(compasParam))
        .on('error', console.error.bind(console))
        ;
});

gulp.task('css:autoprefixer:minify', ['compass'], function () {
    return gulp.src([
        path.join(paths.css, files.css)
    ])
        .pipe(plumber({
            errorHandler: onError
        }))
        .pipe(cssconcat(files.minCss))
        .pipe(autoprefixer({browsers: ['last 2 versions'], cascade: false})) // Создаем префиксы
        .pipe(rename(files.minCss))
        .pipe(minifycss())
        .pipe(gulp.dest(paths.css))
        .pipe(livereload())
        //.pipe(notify({message: 'Autoprefixer task complete'}))
        ;
});

gulp.task('scripts', function () {//['del:js'],
    gulp.src(mainBowerFiles({filter: new RegExp('.*js$', 'i')}))
        .pipe(plumber({
            errorHandler: onError
        }))
        .pipe(concat(files.libsJS))
        .pipe(gulp.dest(paths.tmp));
    // Берем все необходимые библиотеки
    return gulp.src([
        path.join(paths.tmp, files.libsJS),

        path.join(paths.devJs, '/libs/modernizr.min.js'),
        path.join(paths.devJs, '/libs/tether.min.js'),
        path.join(paths.devJs, '/libs/bootstrap.min.js'),

        path.join(paths.devJs, files.js)
    ])
        .pipe(plumber({
            errorHandler: onError
        }))
        .pipe(concat(files.js))
        .pipe(gulp.dest(paths.js))
        .pipe(rename({suffix: '.min'}))
        .pipe(uglify())
        .pipe(gulp.dest(paths.js));
});

gulp.task('img:minification', function () {
    return gulp.src(paths.devImg + '/**/*') // Берем все изображения из app
        .pipe(plumber({
            errorHandler: onError
        }))
        .pipe(imagemin({ // Сжимаем их с наилучшими настройками
            interlaced : true,
            progressive: true,
            svgoPlugins: [{removeViewBox: false}],
            use        : [pngquant()]
        }))
        .pipe(gulp.dest(paths.img));
});

// ----------------------------------------

gulp.task('img:sprite', ['img:minification'], function () {
    var spriteData =
            gulp.src(paths.img + '/sprite/*.*') // путь, откуда берем картинки для спрайта
                .pipe(spritesmith({
                    imgName  : 'sprite.png',
                    cssFormat: 'scss',
                    cssName  : '__sprite.scss',
                    algorithm: 'binary-tree'
                }));

    spriteData.img.pipe(gulp.dest(paths.img)); // путь, куда сохраняем картинку
    spriteData.css.pipe(gulp.dest(paths.scss + '/styles/')); // путь, куда сохраняем стили
});

// ----------------------------------------



/** Configuration **/
var user = '';
var password = '';
var host = '92.53.96.24';
var port = 21;
var remoteFolder = '//public_html';
var localFilesGlob = ['./assets/css/**/*.css','./assets/js/**/*.js','./assets/img/**/*.*'];

// helper function to build an FTP connection based on our configuration
function getFtpConnection() {
    return ftp.create({
        host: host,
        port: port,
        user: user,
        password: password,
        parallel: 5
    });
}

/**
 * Deploy task.
 * Copies the new files to the server
 */
gulp.task('ftp-deploy', function() {

    var conn = getFtpConnection();

    return gulp.src(localFilesGlob, { base: '.', buffer: false })
        .pipe( conn.newer( remoteFolder ) ) // only upload newer files
        .pipe( conn.dest( remoteFolder ) )
        ;
});

// ----------------------------------------


gulp.task('watch', function () {
    livereload.listen();
    gulp.watch(paths.founds.scss, ['css:autoprefixer:minify'])
        .on('change', function (event) {
            console.log('File ' + event.path.replace(__dirname, '').replace(/\\/g, '/') + ' was ' + event.type + ' and reloaded');
        });
    gulp.watch(paths.founds.js, ['scripts'])
        .on('change', function (event) {
            console.log('File ' + event.path.replace(__dirname, '').replace(/\\/g, '/') + ' was ' + event.type + ' and reloaded');
        });
    gulp.watch(paths.founds.html, ['html'])
        .on('change', function (event) {
            console.log('File ' + event.path.replace(__dirname, '').replace(/\\/g, '/') + ' was ' + event.type + ' and reloaded');
        });
/*
    var conn = getFtpConnection();

    gulp.watch(localFilesGlob)
        .on('change', function(event) {
            console.log('Changes detected! Uploading file "' + event.path + '", ' + event.type);

            return gulp.src( [event.path], { base: '.', buffer: false } )
                .pipe( conn.newer( remoteFolder ) ) // only upload newer files
                .pipe( conn.dest( remoteFolder ) )
                ;
        });*/
});

// ----------------------------------------

gulp.task('del:css', function () {
    return del([paths.css + '*']);
});

gulp.task('del:js', function () {
    return del([paths.tmp + '*', paths.js + '*']);
});

// ----------------------------------------

gulp.task('default', ['watch']); //'connect','img:minification','scripts', 'css:autoprefixer:minify',
