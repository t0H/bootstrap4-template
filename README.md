Blank template layout of the site.

### Used ###
* **Css:** *Bootstrap 4 alfa, SASS, Compass.*
* **Js:** *fancybox, jquery.formstyler, jquery.maskedinput, tether.*

[Wiki](https://bitbucket.org/t0H/clear-template/wiki)

bower install
npm install
