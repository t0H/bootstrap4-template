var gulp           = require('gulp'),

    compass        = require('gulp-compass'),
    minifycss      = require('gulp-minify-css'),
    rename         = require('gulp-rename'),

    cssconcat      = require('gulp-concat-css'),
    concat         = require('gulp-concat'), // Подключаем gulp-concat (для конкатенации файлов)
    uglify         = require('gulp-uglifyjs'), // Подключаем gulp-uglifyjs (для сжатия JS)

    imagemin       = require('gulp-imagemin'), // Подключаем библиотеку для работы с изображениями
    pngquant       = require('imagemin-pngquant'), // Подключаем библиотеку для работы с png

    filter         = require('filter'),
    mainBowerFiles = require('main-bower-files'),
    autoprefixer   = require('gulp-autoprefixer'),// Подключаем библиотеку для автоматического добавления префиксов

    livereload     = require('gulp-livereload'),
    connect        = require('gulp-connect'),

    plumber        = require('gulp-plumber'),
    notify         = require('gulp-notify'),

    del            = require('del'),
    path           = require('path'),

    ftp            = require('vinyl-ftp');

// ----------------------------------------

var sendFtp   = false,
    ftpConfig = {
        host        : '',
        port        : 21,
        user        : '',
        password    : '',
        parallel    : 5,
        remoteFolder: '/public_html'
    };

// ----------------------------------------

var onError = function (err) {
    console.log(err);
};

onError = {
    title  : 'Gulp',
    message: "Error: <%= error.message %>"
};

var files          = {
        css          : 'docs.css',
        minCss       : 'docs.min.css',
        libsJS       : 'libs.js',
        js           : "scripts.js",
        minJs        : "scripts.min.js",
        compassConfig: "./config.rb",
        arrCss       : [],
        arrJs        : []
    },
    //['./assets/css/**/*.css', './assets/js/**/*', './assets/img/**/*'];
    localFilesGlob = ['./assets/**/*'];

var paths = {
    assets: "./assets",
    dev   : './source',
    founds: {}
};


paths.css   = path.join(paths.assets, "css");
paths.img   = path.join(paths.assets, "img");
paths.js    = path.join(paths.assets, "js");
paths.fonts = path.join(paths.assets, "fonts");

paths.devImg = path.join(paths.dev, "img");
paths.scss   = path.join(paths.dev, 'sass');
paths.tmp    = path.join(paths.dev, '.tmp');
paths.devJs  = path.join(paths.dev, 'js');

paths.founds = {
    scss  : path.join(paths.scss, '**/*.scss'),
    sass  : path.join(paths.scss, '**/*.sass'),
    js    : path.join(paths.devJs, '**/*.js'),
    devImg: path.join(paths.devImg, '**/*'),
    html  : ['./*.html']
};
files.arrCss = [
    //path.join(paths.devJs, '/libs/fancybox/jquery.fancybox.min.css'),
    path.join(paths.css, files.css)
];

files.arrJs = [
    path.join(paths.tmp, files.libsJS), // bower_components: jquery, jquery.maskedinput

    path.join(paths.devJs, '/libs/modernizr.min.js'),
    path.join(paths.devJs, '/libs/tether.min.js'),
    path.join(paths.devJs, '/libs/bootstrap.min.js'),

    //path.join(paths.devJs, '/libs/fancybox/jquery.fancybox.min.js'),

    path.join(paths.devJs, files.js)
];

var compasParam = {
    //config_file: files.compassConfig,
    sass       : paths.scss,
    css        : paths.css,
    style      : 'nested', // expanded or nested or compact or compressed
    project    : './',
    image      : paths.img,
    javascripts: paths.js,
    comments   : false,
    sourcemap  : false,
    time       : true,
    logging    : false,
    debug      : false
};

// ----------------------------------------

gulp.task('connect', function () {
    return connect.server({
        root      : './',
        livereload: true
    });
});

gulp.task('html', function () {
    return gulp.src(paths.founds.html).pipe(livereload());
});

// ----------------------------------------

gulp.task('compass', function () {//['del:css'],
    return gulp.src([paths.founds.sass])
        .pipe(plumber({errorHandler: onError}))
        .pipe(compass(compasParam).on('error', console.error.bind(console)))
        .on('error', console.error.bind(console))
        ;
});

gulp.task('css:autoprefixer:minify', ['compass'], function () {
    return gulp.src(files.arrCss)
        .pipe(plumber({
            errorHandler: onError
        }))
        .pipe(cssconcat(files.minCss))
        .pipe(autoprefixer({browsers: ['last 2 versions'], cascade: false})) // Создаем префиксы
        .pipe(rename(files.minCss))
        .pipe(minifycss())
        .pipe(gulp.dest(paths.css))
        .pipe(livereload())
        //.pipe(notify({message: 'Autoprefixer task complete'}))
        ;
});

// ----------------------------------------

gulp.task('scripts', function () {//['del:js'],
    gulp.src(mainBowerFiles({filter: new RegExp('.*js$', 'i')}))
        .pipe(plumber({
            errorHandler: onError
        }))
        .pipe(concat(files.libsJS))
        .pipe(gulp.dest(paths.tmp));
    // Берем все необходимые библиотеки
    return gulp.src(files.arrJs)
        .pipe(plumber({
            errorHandler: onError
        }))
        .pipe(concat(files.js))
        .pipe(gulp.dest(paths.js))
        .pipe(rename({suffix: '.min'}))
        .pipe(uglify())
        .pipe(gulp.dest(paths.js));
});

// ----------------------------------------

gulp.task('img:minification', function () {
    return gulp.src(paths.devImg + '/**/*') // Берем все изображения
        .pipe(plumber({
            errorHandler: onError
        }))
        .pipe(imagemin({ // Сжимаем их с наилучшими настройками
            interlaced : true,
            progressive: true,
            svgoPlugins: [{removeViewBox: false}],
            use        : [pngquant()]
        }))
        .pipe(gulp.dest(paths.img))
        .pipe(notify({onLast: true, title: 'img:minification', message: 'Image minification complite!'}))
        ;
});

// ----------------------------------------

gulp.task('watch', function () {
    livereload.listen();
    gulp.watch(paths.founds.scss, ['css:autoprefixer:minify'])
        .on('change', function (event) {
            var filename = event.path.split('\\');
            console.log('\x1b[34m----------\x1b[0m File \x1b[31m' + filename[filename.length - 1] + '\x1b[0m was ' + event.type);
        });
    gulp.watch(paths.founds.js, ['scripts'])
        .on('change', function (event) {
            var filename = event.path.split('\\');
            console.log('\x1b[34m----------\x1b[0m File \x1b[31m' + filename[filename.length - 1] + '\x1b[0m was ' + event.type);
        });
    gulp.watch(paths.founds.html)//, ['html']
        .on('change', function (event) {
            var filename = event.path.split('\\');
            console.log('\x1b[34m----------\x1b[0m File \x1b[31m' + filename[filename.length - 1] + '\x1b[0m was ' + event.type);

            return gulp.src([event.path], {base: '.', buffer: false}).pipe(livereload());
        });


    if (sendFtp) {
        var ftpConnect = ftp.create(ftpConfig);

        gulp.watch(localFilesGlob)
            .on('change', function (event) {
                var filename = event.path.split('\\');
                console.log('\x1b[34m----------\x1b[0m Changes detected! Uploading file: \x1b[31m"' + filename[filename.length - 1] + '"\x1b[0m');

                return gulp.src([event.path], {base: '.', buffer: false})
                //.pipe(ftpConnect.newer(ftpConfig.remoteFolder)) // only upload newer files
                //.pipe(ftpConnect.differentSize(ftpConfig.remoteFolder))
                    .pipe(ftpConnect.dest(ftpConfig.remoteFolder))
                    //.pipe(notify({ onLast: true, message: 'Upload to server complete!'}))
                    ;
            });
    }
});

// ----------------------------------------

gulp.task('del:css', function () {
    return del([paths.css + '*']);
});

gulp.task('del:js', function () {
    return del([paths.tmp + '*', paths.js + '*']);
});

// ----------------------------------------

gulp.task('default', ['watch']); //'connect','img:minification','scripts', 'css:autoprefixer:minify',
